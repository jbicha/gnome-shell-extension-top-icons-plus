#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/default.mk

%:
	dh $@

override_dh_auto_build:
	make build

override_dh_auto_install:
	:

ORIG_REPO ?= https://github.com/phocean/TopIcons-plus
ORIG_COMMIT = ead46bc42e7cc93e98bc95d9e6dfa04d87f11433
# Use the date of the ORIG_COMMIT, or 20130426.1 if you snapshot twice in a day,
# or empty if ORIG_COMMIT is
ORIG_DATE = 20170827

ORIG_VER := 20+git${ORIG_DATE}
ORIG_EXPORT := ${ORIG_COMMIT}

get-orig-source:
	test ! -e ${DEB_SOURCE}-${ORIG_VER}.git
	test ! -e ${DEB_SOURCE}_${ORIG_VER}.orig
	test ! -e ${DEB_SOURCE}_${ORIG_VER}.orig.tar.xz
	git clone --bare ${ORIG_REPO} ${DEB_SOURCE}-${ORIG_VER}.git
	GIT_DIR=${DEB_SOURCE}-${ORIG_VER}.git git archive \
		--format=tar \
		--prefix=${DEB_SOURCE}-${ORIG_VER}.orig/ \
		${ORIG_EXPORT} | tar -xvf-
	LC_ALL=C TZ=UTC GIT_DIR=${DEB_SOURCE}-${ORIG_VER}.git git log ${ORIG_EXPORT} \
	       > ${DEB_SOURCE}-${ORIG_VER}.orig/ChangeLog
	tar --xz -cvf ${DEB_SOURCE}_${ORIG_VER}.orig.tar.xz ${DEB_SOURCE}-${ORIG_VER}.orig
	rm -rf ${DEB_SOURCE}-${ORIG_VER}.orig
	rm -rf ${DEB_SOURCE}-${ORIG_VER}.git

# to be invoked from a git checkout with upstream github as a remote;
# tarball ends up in ../build-area
maintainer-get-orig-source:
	$(MAKE) -f debian/rules get-orig-source ORIG_REPO=$$(pwd)/.git
	mv -i -v ${DEB_SOURCE}_${ORIG_VER}.orig.tar.xz ../build-area/
	@echo "try using:"
	@echo "gbp import-orig --debian-branch=debian/master --upstream-branch=upstream/latest --upstream-vcs-tag=${ORIG_EXPORT} ../build-area/${DEB_SOURCE}_${ORIG_VER}.orig.tar.xz"
